/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicadyv;

import java.util.*;
import java.lang.*;
/**
 *
 * @author Universidad
 */
public class PracticaDyV {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner teclado = new Scanner(System.in);
        ConjuntoDePuntos conjPuntos = null;
        ParDePuntos parPuntos = null;
        boolean salir, debug = false;
        
        System.out.print("Programa para calcular el par de puntos mas proximo.\n" + 
                         "Desea introducir los puntos manualmente(m) o automaticamente(a).\n" + 
                         "Tambien se puede comprobar la eficacia del algoritmo(d).\n");
        
        salir = false;
        while (!salir) {
            System.out.print("Opcion: ");
            switch (teclado.next().charAt(0) ) {
                case 'm':
                case 'M':
                    conjPuntos = insercionManual();
                    salir = true;
                    break;
                    
                case 'd':
                case 'D':
                    debug = true;
                case 'a':
                case 'A':
                    conjPuntos = insercionAutomatica();
                    salir = true;
                    break;
                    
                default:
                    System.out.println("La opcion elegida no es valida.");
            }
        }
        
        System.out.println("Los puntos se han guardado correctamente.");
        
        long end;
        
        if (!debug) {
            end = System.currentTimeMillis();
            parPuntos = conjPuntos.hayarDistanciaMinima();
            end = System.currentTimeMillis() - end;

            System.out.println("Los puntos con distancia minima son P1(" + parPuntos.getA().x + ", " + parPuntos.getA().y + 
                               ") y P2(" + parPuntos.getB().x + ", " + parPuntos.getB().y + ") y ha tardado " + end + "ms. Dist = " + parPuntos.getDistancia() + ".\n");

            end = System.currentTimeMillis();
            parPuntos = conjPuntos.hayarDistanciaMinimaFB();
            end = System.currentTimeMillis() - end;

            System.out.println("Los puntos con distancia minima por fuerza bruta son P1(" + parPuntos.getA().x + ", " + parPuntos.getA().y + 
                               ") y P2(" + parPuntos.getB().x + ", " + parPuntos.getB().y + ") y ha tardado " + end + "ms. Dist = " + parPuntos.getDistancia() + "\n");        
        } else {
            double dist;
            long i = 0;
            while (true) {
                dist = conjPuntos.hayarDistanciaMinima().getDistancia() - conjPuntos.hayarDistanciaMinimaFB().getDistancia();
                if (dist != 0)
                    System.out.println("Error en el paso " + i + " con una distancia de " + dist);
                if (i % 10 == 0) {
                    System.out.println("Paso " + i);
                }
                i++;
            }
        }
    }
    
    public static ConjuntoDePuntos insercionManual() {
        Punto[] puntos = null;
        double x, y;
        int n = 0;
        
        n = obtenerInt("Cuantos puntos quiere introducir: ");
        puntos = new Punto[n];
        for(int i=0; i<n; i++) {
            System.out.println("  Punto " + i);
            x = obtenerDouble("  - Coordenada X: ");
            y = obtenerDouble("  - Coordenada Y: ");
                    
            puntos[i] = new Punto(x, y);
        }
        
        return new ConjuntoDePuntos(puntos);
    }
    
    public static ConjuntoDePuntos insercionAutomatica() {
        Punto[] puntos = null;
        int n = 0;
        double nMin = 0, nMax = 0, nRango = 0;
        
        n = obtenerInt("Cuantos puntos quiere introducir: ");
        puntos = new Punto[n];
        
        do {
          nMin = obtenerDouble("Que valor quiere que tengan como minimo los valores: ");
          nMax = obtenerDouble("Que valor quiere que tengan como maximo los valores: ");
          if (nMin > nMax)
              System.out.println("El limite inferior no puede ser mayor al superior.");
        } while (nMin > nMax);
        
        nRango = nMax - nMin;
        for(int i=0; i<n; i++) {
            puntos[i] = new Punto(nMin + Math.random()*nRango, nMin + Math.random()*nRango);
        }
        
        return new ConjuntoDePuntos(puntos);
    }
    
    // Metodo el cual pregunta por un numero hasta que se introduce uno valido
    public static  int obtenerInt(String mensaje) {
            int numero = 0;
            boolean salir = false;
            Scanner teclado = new Scanner(System.in);
            String s = new String("");

            while (!salir) {
                    System.out.print(mensaje);
                    s = teclado.next();

                    // mientras que el programa no llegue a la variable salir se repetira este
                    try {
                            numero = Integer.parseInt(s);
                            salir = true;
                    }
                    catch (NumberFormatException e) {
                            System.out.print("(ERROR) El numero introducido no es valido.\n");
                    }
            }  

            return numero; // devolvemos el numero obtenido
    }
    

    // Metodo el cual pregunta por un numero hasta que se introduce uno valido
    public static double obtenerDouble(String mensaje) {
            double numero = 0;
            boolean salir = false;
            Scanner teclado = new Scanner(System.in);
            String s = new String("");

            while (!salir) {
                    System.out.print(mensaje);
                    s = teclado.next();

                    // mientras que el programa no llegue a la variable salir se repetira este
                    try {
                            numero = Double.parseDouble(s);
                            salir = true;
                    }
                    catch (NumberFormatException e) {
                            System.out.print("(ERROR) El numero introducido no es valido.\n");
                    }
            }  

            return numero; // devolvemos el numero obtenido
    }
}
