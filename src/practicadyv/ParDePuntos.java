/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicadyv;

import java.awt.geom.Point2D;
import java.util.*;
import java.lang.*;
/**
 *
 * @author Universidad
 */
public class ParDePuntos {
    // declaramos las variables de la clase
    private Punto _puntoA;
    private Punto _puntoB;
    private double _distancia;
    
    // ponemos que la distancia entre los mismos es infinita
    public ParDePuntos() {
        _puntoA = null;
        _puntoB = null;
        _distancia = Double.MAX_VALUE;
    }
    
    public ParDePuntos(Punto a, Punto b) {
        _puntoA = a;
        _puntoB = b;
        calcularDistanciaCuadrado();
    }
    
    public ParDePuntos(Punto a, Punto b, double distance) {
        _puntoA = a;
        _puntoB = b;
    }
    
    public Punto getA() {
        return _puntoA;
    }
    
    public Punto getB() {
        return _puntoB;
    }
    
    public double getDistancia() {
        return _distancia;
    }
    
    public void setA( Punto a ) {
        _puntoA = a;
    }
    
    public void setB( Punto b ) {
        _puntoB = b;
    }
    
    public void setDistancia( double distancia ) {
        _distancia = distancia;
    }
    
    public void calcularDistanciaCuadrado() {
        double x = _puntoA.x - _puntoB.x;
        double y = _puntoA.y - _puntoB.y;
        _distancia = x*x + y*y;
    }
    
    public double raizCuadradaDistancia() {
        return Math.sqrt(_distancia);
    }
    
    public static ParDePuntos elegirPuntosDistanciaMin(Punto a, Punto b, Punto c) {       
        return new ParDePuntos( a, b ).guardarSiEsMinimo(
                new ParDePuntos( b, c ).guardarSiEsMinimo(
                new ParDePuntos( a, c ) ) );
    }
    
    public ParDePuntos guardarSiEsMinimo( ParDePuntos p ) {
        if (p.getDistancia() < this.getDistancia()) {
            this.setA(p.getA());
            this.setB(p.getB());
            this.setDistancia(p.getDistancia());
        }
        
        return this;
    }
}
