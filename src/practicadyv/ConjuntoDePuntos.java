/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicadyv;

import java.util.*;
import java.lang.*;

/**
 *
 * @author Universidad
 */
public class ConjuntoDePuntos {
    // declaramos las variables de la clase
    private Punto[] _puntos = null;

    public ConjuntoDePuntos(Punto[] puntos) {
        _puntos = puntos;
    }
    
    public ParDePuntos hayarDistanciaMinima() {
        if (_puntos.length <= 1) return null;
        
        Punto[] vectTmp = _puntos.clone();
        
        ordenarPuntosQS(vectTmp, 0, vectTmp.length-1);
        ParDePuntos resultado = hayarDistanciaMinima(vectTmp, 0, vectTmp.length-1);
        resultado.setDistancia( resultado.raizCuadradaDistancia() );
        return resultado;
    }
    
    public ParDePuntos hayarDistanciaMinimaFB() {
        if (_puntos.length <= 1) return null;
        ParDePuntos resultado = hayarDistanciaMinimaFB(_puntos, 0, _puntos.length-1);
        resultado.setDistancia( resultado.raizCuadradaDistancia() );
        return resultado;
    }

    private void ordenarPuntosQS(Punto[] vect, int a, int b) {
        if (b-a<1) return;
        int med = (a+b)/2;
        
        // 'evitamos' que si el vector esta ordenado la potencia del QC baje
        Punto p = vect[med];
        vect[med] = vect[a];
        
        med = QSMitad(a+1, b, p.x, vect );
        
        vect[a] = vect[med];
        vect[med] = p;
        
        ordenarPuntosQS(vect, a, med-1);
        ordenarPuntosQS(vect, med+1, b);
    }
    
    private int BuscarPuntoX(int a, int b, double valorPiv, Punto[] aPuntos) {
        int med = a;
        while (a < b) {
            med = (a+b)/2;
            if ( aPuntos[med].x < valorPiv ) {
                a = med+1;
            } else {
                b = med-1;
            }
        }
        
        return ( aPuntos[med].x <= valorPiv ) ? med : med-1;
    }
    
    private int QSMitad(int a, int b, double valorPiv, Punto[] aPuntos) {
        Punto p;
        
        while (a <= b) {
            if ( aPuntos[a].x < valorPiv ) {
                a++;
                continue;
            }

            
            if ( aPuntos[b].x > valorPiv ) {
                b--;
                continue;
            }
            
            p = aPuntos[a];
            aPuntos[a] = aPuntos[b];
            aPuntos[b] = p;
            a++;
            b--;
        }
        
        return b;
    }
    
    private ParDePuntos hayarDistanciaMinimaFB(Punto[] aPuntos, int a, int b) {
        ParDePuntos resultado = new ParDePuntos();
        
        for (int i=a; i<=b; i++)
            for (int j=i+1; j<=b; j++)
                resultado.guardarSiEsMinimo( new ParDePuntos(aPuntos[i], aPuntos[j]) );
        
        return resultado;
    }
    
    private ParDePuntos hayarDistanciaMinima(Punto[] vect, int a, int b) {
        ParDePuntos resultado = new ParDePuntos();
        if (a >= b) return resultado;
        int medio = (a+b)/2;
        
        switch(b-a) {
            case 1:
                resultado = new ParDePuntos(vect[a], vect[b]);
                break;
                
            case 2:
                resultado = ParDePuntos.elegirPuntosDistanciaMin(vect[a], vect[medio], vect[b]);
                break;
                
            default:
                resultado.guardarSiEsMinimo( hayarDistanciaMinima(vect, a, medio) );
                resultado.guardarSiEsMinimo( hayarDistanciaMinima(vect, medio+1, b) );
                
                double dist = resultado.raizCuadradaDistancia();
                int aTmp = BuscarPuntoX(a, medio, vect[medio].x - dist, vect );
                if ( aTmp < a ) aTmp = a;
                int bTmp = BuscarPuntoX(medio+1, b, vect[medio].x + dist, vect )+1;
                if ( bTmp > b ) bTmp = b;
                
                resultado.guardarSiEsMinimo( ( (a != aTmp) || (b != bTmp) ) ?
                        hayarDistanciaMinima(vect, aTmp, bTmp) : 
                        hayarDistanciaMinimaFB(vect, aTmp, bTmp) );
        }
        
        return resultado;
    }
}
